/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/SdlApplication.h>

int main(int argc, char* argv[])
{
	/*
	 * Create an SDL application with "01-application" as the name and
	 * "Suttung Digital" as the organization.
	 */
	auto application = nox::app::SdlApplication{"01-application", "Suttung Digital"};

	/*
	 * Initialize the application. This will initialize SDL and the base nox::app::Applciation class.
	 * You should see some log output from the console when calling the function saying that the
	 * application is initializing.
	 */
	if (application.init(argc, argv) == false)
	{
		return 1;
	}

	/*
	 * Normally you would run application.execute() here to actually run the application, but for
	 * simplicity this is omitted here. It will be explained in a later example.
	 */

	/*
	 * Shutdown the application. This will shutdown SDL and the base nox::app::Applciation class.
	 * You should see some log output from the console when calling the function saying that the
	 * application is shutting down.
	 */
	application.shutdown();

	// Successfully return.
	return 0;
}
