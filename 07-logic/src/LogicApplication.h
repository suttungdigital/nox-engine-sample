/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef LOGICAPPLICATION_H_
#define LOGICAPPLICATION_H_

#include <nox/app/SdlApplication.h>
#include <nox/app/log/Logger.h>

/*
 * This example shows how to set up and run the logic system.
 *
 * The logic is meant to handle any game logic and is run as a subsystem of
 * the application.
 * It should be independent of the platform it runs on and use the application as
 * an abstract layer to the OS.
 * The logic manages several other subsystems that will be explained in later examples.
 *
 * You should see "[verbose][NoxLogic] Initialized." in the console output when run.
 * It will then keep executing until the application is aborted. You should then see
 * "[verbose][NoxLogic] Destroyed.".
 *
 * See the LogicApplication.cpp file for more.
 */
class LogicApplication: public nox::app::SdlApplication
{
public:
	LogicApplication();

	bool onInit() override;

private:
	void initializeLogic();

	nox::app::log::Logger log;
};

#endif
