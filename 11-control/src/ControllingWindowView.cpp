/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "ControllingWindowView.h"

#include <nox/app/graphics/2d/IRenderer.h>
#include <nox/app/graphics/2d/BackgroundGradient.h>
#include <nox/app/IContext.h>
#include <nox/app/resource/IResourceAccess.h>
#include <nox/logic/IContext.h>
#include <nox/logic/graphics/event/DebugRenderingEnabled.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/physics/Simulation.h>

ControllingWindowView::ControllingWindowView(nox::app::IContext* applicationContext, const std::string& windowTitle):
	nox::window::RenderSdlWindowView(applicationContext, windowTitle),
	eventBroadcaster(nullptr),
	renderer(nullptr),
	camera(std::make_shared<nox::app::graphics::Camera>(this->getWindowSize())),
	listener("ControllingWindowView")
{
	this->log = applicationContext->createLogger();
	this->log.setName("ControllingWindowView");

	this->listener.addEventTypeToListenFor(nox::logic::graphics::SceneNodeEdited::ID);

	// We want the camera to follow our controlled Actor, so we listen for TransformChanges.
	this->listener.addEventTypeToListenFor(nox::logic::actor::TransformChange::ID);

	this->camera->setScale({60.0f, 60.0f});
	this->rootSceneNode = std::make_shared<nox::app::graphics::TransformationNode>();
}

bool ControllingWindowView::initialize(nox::logic::IContext* context)
{
	if (this->RenderSdlWindowView::initialize(context) == false)
	{
		return false;
	}

	this->listener.setup(this, context->getEventBroadcaster(), nox::logic::event::ListenerManager::StartListening_t());

	// We store the event::IBrodcaster.
	this->eventBroadcaster = context->getEventBroadcaster();

	/*
	 * The remaining function body loads the "controls.json" file and gives it to the controlMapper so
	 * that it can know how to map the controls. See the controls.json file for more.
	 */

	auto resourceAccess = this->getApplicationContext()->getResourceAccess();

	auto controlLayoutResourceDescriptor = nox::app::resource::Descriptor{"controls.json"};
	auto controlLayoutResource = resourceAccess->getHandle(controlLayoutResourceDescriptor);

	if (controlLayoutResource == nullptr)
	{
		this->log.error().format("Failed loading control layout resource \"%s\".", controlLayoutResourceDescriptor.getPath().c_str());
		return false;
	}
	else
	{
		this->controlMapper.loadKeyboardLayout(controlLayoutResource);
	}

	return true;
}

void ControllingWindowView::onRendererCreated(nox::app::graphics::IRenderer* renderer)
{
	assert(renderer != nullptr);

	const auto graphicsResourceDescriptor = nox::app::resource::Descriptor{"graphics/graphics.json"};
	renderer->loadTextureAtlases(graphicsResourceDescriptor, this->getApplicationContext()->getResourceAccess());
	renderer->setWorldTextureAtlas("graphics/testTextureAtlas");

	auto background = std::make_unique<nox::app::graphics::BackgroundGradient>();
	background->setBottomColor({0.0f, 0.0f, 0.0f});
	background->setTopColor({1.0f, 1.0f, 1.0f});
	renderer->setBackgroundGradient(std::move(background));

	renderer->setAmbientLightLevel(0.0f);
	renderer->setCamera(this->camera);
	renderer->setRootSceneNode(this->rootSceneNode);

	renderer->organizeRenderSteps();

	this->renderer = renderer;
}

/*
 * This is called from nox::logic::View when it has changed its controlled Actor (from e.g. loading the world).
 */
void ControllingWindowView::onControlledActorChanged(nox::logic::actor::Actor* controlledActor)
{
	/*
	 * We make shure that our controlMapper knows which Actor it is controlling so that the events are
	 * sent correctly.
	 */
	this->controlMapper.setControlledActor(controlledActor);
}

void ControllingWindowView::onWindowSizeChanged(const glm::uvec2& size)
{
	this->RenderSdlWindowView::onWindowSizeChanged(size);

	this->camera->setSize(size);
}

void ControllingWindowView::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	this->RenderSdlWindowView::onEvent(event);

	using SceneNodeEdit = nox::logic::graphics::SceneNodeEdited;

	if (event->isType(SceneNodeEdit::ID))
	{
		auto nodeEvent = static_cast<SceneNodeEdit*>(event.get());

		if (nodeEvent->getEditAction() == SceneNodeEdit::Action::CREATE)
		{
			this->rootSceneNode->addChild(nodeEvent->getSceneNode());
		}
		else if (nodeEvent->getEditAction() == SceneNodeEdit::Action::REMOVE)
		{
			this->rootSceneNode->removeChild(nodeEvent->getSceneNode());
		}
	}
	else if (event->isType(nox::logic::actor::TransformChange::ID))
	{
		const auto transformEvent = static_cast<nox::logic::actor::TransformChange*>(event.get());

		/*
		 * If the transformed Actor is our controlled Actor, we want to move and rotate the camera to its new position
		 * and rotation.
		 */
		if (transformEvent->getActor() == this->getControlledActor())
		{
			this->camera->setPosition(transformEvent->getPosition());
			this->camera->setRotation(transformEvent->getRotation());
		}
	}
}

void ControllingWindowView::onKeyPress(const SDL_KeyboardEvent& event)
{
	if ((event.keysym.mod & KMOD_CTRL) && event.keysym.sym == SDLK_q && this->renderer != nullptr)
	{
		this->renderer->toggleDebugRendering();

		const auto debugRenderEvent = std::make_shared<nox::logic::graphics::DebugRenderingEnabled>(renderer->isDebugRenderingEnabled());
		this->getLogicContext()->getEventBroadcaster()->queueEvent(debugRenderEvent);
	}

	/*
	 * If our controlMapper has a controlled Actor (see onControlledActorChanged()), we map the key
	 * press to control::Action events. The mapping is based on the JSON that was parsed in initialize().
	 * We then broadcast all of these events to the logic so that the Actor can handle the controls.
	 *
	 * All events with the "move" action name will be handled by the Actor2dDirectionControl.
	 * All events with the "rotate" action name will be handled by the Actor2dRotationControl.
	 * You can implement more control components by inheriting the control::ActorControl class
	 * and handling control events of your desired names.
	 */
	if (this->controlMapper.hasControlledActor() == true)
	{
		auto controlEvents = this->controlMapper.mapKeyPress(event.keysym);

		if (this->eventBroadcaster != nullptr)
		{
			for (const auto& event : controlEvents)
			{
				this->eventBroadcaster->queueEvent(event);
			}
		}
	}
}

void ControllingWindowView::onKeyRelease(const SDL_KeyboardEvent& event)
{
	/*
	 * The same as in onKeyPress(), only that we map a key release in stead of a key press.
	 */
	if (this->controlMapper.hasControlledActor() == true)
	{
		auto controlEvents = this->controlMapper.mapKeyRelease(event.keysym);

		if (this->eventBroadcaster != nullptr)
		{
			for (const auto& event : controlEvents)
			{
				this->eventBroadcaster->queueEvent(event);
			}
		}
	}
}
