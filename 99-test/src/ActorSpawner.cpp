/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "ActorSpawner.h"

#include <nox/util/json_utils.h>
#include <nox/util/chrono_utils.h>
#include <nox/logic/IContext.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/physics/actor/ActorPhysics.h>

const ActorSpawner::IdType ActorSpawner::NAME = "Spawner";

ActorSpawner::~ActorSpawner() = default;

bool ActorSpawner::initialize(const Json::Value& componentJsonObject)
{
	this->spawnedActor = componentJsonObject.get("spawnedActor", "").asString();
	this->spawnVelocity = nox::util::parseJsonVec(componentJsonObject["spawnVelocity"], glm::vec2(0.0f, 0.0f));
	this->spawnAsFriend = componentJsonObject.get("spawnAsFriend", true).asBool();

	const auto spawnInterval = componentJsonObject.get("spawnInterval", 1.0f).asFloat();
	this->spawnTimer.setTimerLength(nox::util::secondsToDuration<nox::Duration>(spawnInterval));

	return true;
}

void ActorSpawner::serialize(Json::Value& componentObject)
{
}

void ActorSpawner::onCreate()
{
	this->spawnTimer.reset();
}

void ActorSpawner::onUpdate(const nox::Duration& deltaTime)
{
	this->spawnTimer.spendTime(deltaTime);

	if (this->spawnTimer.timerReached())
	{
		this->spawnActor();
		this->spawnTimer.reset();
	}
}

const ActorSpawner::IdType& ActorSpawner::getName() const
{
	return NAME;
}

void ActorSpawner::spawnActor()
{
	this->getLog().debug().raw("Spawning Actor.");

	auto actor = this->getLogicContext()->getWorldManager()->createActorFromDefinitionName(this->spawnedActor);

	if (actor == nullptr)
	{
		this->getLog().error().format("Could not spawn Actor from definition \"%s\".", this->spawnedActor.c_str());
	}
	else
	{
		auto spawnerTransform = this->getOwner()->findComponent<nox::logic::actor::Transform>();
		auto spawneeTransform = actor->findComponent<nox::logic::actor::Transform>();
		if (spawnerTransform && spawneeTransform)
		{
			spawneeTransform->setPosition(spawnerTransform->getPosition());
			spawneeTransform->setRotation(spawnerTransform->getRotation());
		}

		auto spawneePhysics = actor->findComponent<nox::logic::physics::ActorPhysics>();
		if (spawneePhysics)
		{
			spawneePhysics->setVelocity(this->spawnVelocity);

			if (this->spawnAsFriend)
			{
				spawneePhysics->setFriendActorGroup(this->getOwner()->getId());
				this->getLog().debug().raw("Set spawed Actor to be friend with spawner.");
			}
		}

		this->getLogicContext()->getWorldManager()->manageActor(std::move(actor));

		this->getLog().debug().raw("Actor spawned.");
	}
}
