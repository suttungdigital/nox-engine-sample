/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "UpdateApplication.h"

UpdateApplication::UpdateApplication():
	SdlApplication("03-updating", "SuttungDigital")
{
}

bool UpdateApplication::onInit()
{
	if (SdlApplication::onInit() == false)
	{
		return false;
	}

	this->log = this->createLogger();
	this->log.setName("UpdateApplication");

	// The timer is set to be 500ms long.
	this->outputTimer.setTimerLength(std::chrono::milliseconds(500));

	// Returning true here will allow the application to execute.
	return true;
}

void UpdateApplication::onUpdate(const nox::Duration& deltaTime)
{
	SdlApplication::onUpdate(deltaTime);

	/*
	 * The deltaTime passed is the real time since the previous update.
	 * Adding the deltaTimes together over the whole application lifetime
	 * will end up to be the actual time the application has run.
	 *
	 * So here we add the deltaTime to our outputTimer so that it knows how
	 * much time has been spent. When the time spent reaches 500ms or more,
	 * the timer is done.
	 */
	this->outputTimer.spendTime(deltaTime);

	/*
	 * If this if check passes, 500ms has gone, so we can do our output.
	 */
	if (this->outputTimer.timerReached() == true)
	{
		/*
		 * Print a message, together with the TPS. The TPS is the Ticks Per Seconds
		 * and indicates how many times the application is updated over a second.
		 */
		this->log.info().format("500ms has gone. The TPS is %.2f", this->getTps());

		/*
		 * We reset the timer back to 0 so that we can wait another 500ms for the next output.
		 * Without this, the time would just accumulate and this if check would pass each time,
		 * producing a lot of output.
		 */
		this->outputTimer.reset();
	}
}
