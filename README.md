# README #
Sample application for using the [Nox Engine](https://bitbucket.org/suttungdigital/nox-engine).

To build and run the project, apply the [instructions from NOX Engine](https://bitbucket.org/suttungdigital/nox-engine/src/master/README.md) to this project.

It is crucial that the working directory is set to the root directory (where the assets directory is) so that the application can find the assets.
You do this by running the executable from a terminal in the root directory, or by setting the working directory in your IDE (google it).
