/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "AudioApplication.h"

#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>
#include <nox/app/resource/loader/OggLoader.h>
#include <nox/app/resource/data/SoundExtraData.h>
#include <nox/app/audio/openal/OpenALSystem.h>
#include <nox/app/audio/PlaybackProcess.h>

#include <json/value.h>
#include <cassert>

AudioApplication::AudioApplication():
	SdlApplication("05-audio", "SuttungDigital")
{
}

/*
 * We update the resource cache like in the previous example, so that we can get the sound resource.
 * This version uses an OggLoader in stead of the JsonLoader, so that it can load ogg/vorbis files.
 */
bool AudioApplication::initializeResourceCache()
{
	const auto cacheSizeMb = 512u;
	auto resourceCache = std::make_unique<nox::app::resource::LruCache>(cacheSizeMb);

	resourceCache->setLogger(this->createLogger());

	const auto assetDirectory = std::string{"assets/"};
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(assetDirectory)) == false)
	{
		return false;
	}

	resourceCache->addLoader(std::make_unique<nox::app::resource::OggLoader>());

	this->setResourceCache(std::move(resourceCache));

	return true;
}

/*
 * We call this function to initialize our audio system.
 */
bool AudioApplication::initializeAudioSystem()
{
	/*
	 * Create the audio system. NOX currently only supports OpenAL, so this will
	 * be what we use.
	 */
	auto audioSystem = std::make_unique<nox::app::audio::OpenALSystem>();

	/*
	 * Initialize the audio system and return false if it failed to abort the initialization.
	 */
	if (audioSystem->initialize() == false)
	{
		return false;
	}

	/*
	 * Give the audio system on the Application base class, so that it can manage it.
	 * The audio system can then be accessed through the nox::app::IContext::getAudioSystem()
	 * function, which you will see below. It will be automatically destroyed when the application
	 * shuts down.
	 */
	this->setAudioSystem(std::move(audioSystem));

	return true;
}

bool AudioApplication::onInit()
{
	if (SdlApplication::onInit() == false)
	{
		return false;
	}

	this->log = this->createLogger();
	this->log.setName("AudioApplication");

	if (this->initializeResourceCache() == false)
	{
		this->log.error().raw("Failed to initialize resource cache.");
		return false;
	}

	/*
	 * Initialize the audio system and return false if it failed.
	 */
	if (this->initializeAudioSystem() == false)
	{
		this->log.error().raw("Failed to initialize audio system.");
		return false;
	}

	auto resourceAccess = this->getResourceAccess();

	const auto soundFileDescriptor = nox::app::resource::Descriptor{"sound.ogg"};
	auto soundResource = resourceAccess->getHandle(soundFileDescriptor);

	if (soundResource != nullptr)
	{
		/*
		 * We get the audioSystem from the nox::app::IContext interface (which we inherit from).
		 */
		nox::app::audio::System* audioSystem = this->getAudioSystem();
		assert(audioSystem != nullptr);

		/*
		 * We the start a playback process to play the sound. The process takes the resource and the audio system, and
		 * then automatically loads the resource into the system. The last argument (true) says that the sound should keep
		 * looping.
		 * When added to the processManager, the manager will manage the process and start it when it is next updated.
		 */
		auto playBackProcess = std::make_unique<nox::app::audio::PlaybackProcess>(soundResource, audioSystem, true);
		this->processManager.startProcess(std::move(playBackProcess));

		this->log.info().format("Started playing sound: %s", soundFileDescriptor.getPath().c_str());
	}
	else
	{
		this->log.error().format("Could not load resource: %s", soundFileDescriptor.getPath().c_str());
	}

	return true;
}

/**
 * We need to update the application so that we can update our process::Manager.
 */
void AudioApplication::onUpdate(const nox::Duration& deltaTime)
{
	SdlApplication::onUpdate(deltaTime);

	/*
	 * The process manager will update all of its processes. In this example there is only the
	 * PlaybackProcess.
	 *
	 * The manager will automatically start the process, in turn starting the sound, if it is not already started,
	 * then it will update it each tick by calling this function.
	 *
	 * Since the PlaybackProcess is looping, the process will not end until the application is shut down.
	 */
	this->processManager.updateProcesses(deltaTime);
}
