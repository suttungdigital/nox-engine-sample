/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef AUDIOAPPLICATION_H_
#define AUDIOAPPLICATION_H_

#include <nox/app/SdlApplication.h>
#include <nox/app/log/Logger.h>

/*
 * This example shows how to set up an audio system and how
 * to play sounds with it.
 *
 * For this example it is extremely important that the working directory
 * is set to the project directory (the one with all the example directories
 * and an assets directory). This is so that the resource cache can find
 * the assets directory.
 *
 * See the AudioApplication.cpp file for more.
 */
class AudioApplication: public nox::app::SdlApplication
{
public:
	AudioApplication();

	bool onInit() override;
	void onUpdate(const nox::Duration& deltaTime) override;

private:
	bool initializeResourceCache();
	bool initializeAudioSystem();

	nox::app::log::Logger log;

	// We use a processManager to play our sound.
	nox::process::Manager processManager;
};

#endif
