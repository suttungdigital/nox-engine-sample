/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef TPSUPDATED_H_
#define TPSUPDATED_H_

#include <nox/logic/event/Event.h>

/*
 * Since we want to get the TPS with the event, we need to subclass nox::logic::event::Event
 * and provide functionality to receive the TPS.
 */
class TpsUpdated: public nox::logic::event::Event
{
public:
	/*
	 * The ID uniquely defines an event and should not collide with any other events.
	 * It is not important how or where this ID is defined, but by convention we recommend
	 * to define it as a static public member with the name ID.
	 */
	static const IdType ID;

	/*
	 * Construct the event with the TPS that we want to broadcast.
	 */
	TpsUpdated(const float tps);

	/*
	 * Get the TPS that was broadcasted.
	 */
	float getTps() const;

private:
	// Store the TPS.
	float tps;
};

#endif
