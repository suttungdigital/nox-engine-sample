get_filename_component(SAMPLE_NAME "${CMAKE_CURRENT_LIST_DIR}" NAME)
project(${SAMPLE_NAME})

project(${SAMPLE_NAME})

set(SOURCES
	src/main.cpp
	src/WindowApplication.h
	src/WindowApplication.cpp
	src/ExampleWindowView.h
	src/ExampleWindowView.cpp
)

add_executable(${SAMPLE_NAME} ${SOURCES})
target_link_libraries(${SAMPLE_NAME} ${NOXSAMPLE_NOX_LIBRARY})
