/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef EXAMPLEWINDOWVIEW_H_
#define EXAMPLEWINDOWVIEW_H_

#include <nox/app/graphics/2d/Camera.h>
#include <nox/app/graphics/2d/TransformationNode.h>
#include <nox/window/RenderSdlWindowView.h>

/*
 * This window view inherits from the nox::window::RenderSdlWindowView.
 * The nox::window::RenderSdlWindowView manages a window with SDL2 and runs a nox::app::graphics::OpenGlRenderer
 * to render things to the screen.
 */
class ExampleWindowView final: public nox::window::RenderSdlWindowView
{
public:
	/**
	 * Create an example window view.
	 * @param applicationContext The context that it is created in. This is needed to create a logger and pass it down
	 * the inheritance tree.
	 * @param windowTitle The tilte that will appear on top of the window.
	 */
	ExampleWindowView(nox::app::IContext* applicationContext, const std::string& windowTitle);

private:
	void onRendererCreated(nox::app::graphics::IRenderer* renderer) override;
	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	bool initialize(nox::logic::IContext* context) override;
	void onWindowSizeChanged(const glm::uvec2& size) override;
	void onKeyPress(const SDL_KeyboardEvent& event) override;

	nox::app::log::Logger log;
	nox::logic::event::ListenerManager listener;

	nox::app::graphics::IRenderer* renderer;

	// The camera decides how to project the content on the screen. This is an orthographic projection.
	std::shared_ptr<nox::app::graphics::Camera> camera;

	// The root scene node is where all other scene nodes are attached for rendering.
	std::shared_ptr<nox::app::graphics::TransformationNode> rootSceneNode;
};

#endif
