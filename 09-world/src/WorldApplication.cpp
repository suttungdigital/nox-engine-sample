/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "WorldApplication.h"

#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>
#include <nox/app/resource/loader/JsonLoader.h>
#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/world/Loader.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/physics/box2d/Box2DSimulation.h>
#include <nox/logic/physics/actor/ActorPhysics.h>

#include <json/value.h>
#include <glm/gtx/string_cast.hpp>
#include <cassert>

WorldApplication::WorldApplication():
	SdlApplication("09-world", "SuttungDigital"),
	eventListener(this->getName())
{
}

bool WorldApplication::initializeResourceCache()
{
	const auto cacheSizeMb = 512u;
	auto resourceCache = std::make_unique<nox::app::resource::LruCache>(cacheSizeMb);

	resourceCache->setLogger(this->createLogger());

	// We need to get resources from the assets directory inside this project directory.
	const auto assetDirectory = this->getName() + "/assets";
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(assetDirectory)) == false)
	{
		this->log.error().format("Could not initialized resource cache to \"%s\".", assetDirectory.c_str());
		return false;
	}

	resourceCache->addLoader(std::make_unique<nox::app::resource::JsonLoader>(this->createLogger()));

	this->setResourceCache(std::move(resourceCache));

	return true;
}

nox::logic::Logic* WorldApplication::initializeLogic()
{
	auto logic = std::make_unique<nox::logic::Logic>();
	auto logicPtr = logic.get();

	this->addProcess(std::move(logic));

	return logicPtr;
}

/*
 * We demonstrate the use of Actors with physics so we need a physics simulation.
 */
nox::logic::physics::Simulation* WorldApplication::initializePhysics(nox::logic::Logic* logic)
{
	// Create a Box2D physics simulation.
	auto physics = std::make_unique<nox::logic::physics::Box2DSimulation>(logic);
	physics->setLogger(this->createLogger());

	auto physicsPtr = physics.get();

	// Let the logic handle the physics simulation.
	logic->setPhysics(std::move(physics));

	return physicsPtr;
}

/*
 * The world is handled by the world::Manager so we need to create and initialize it.
 */
nox::logic::world::Manager* WorldApplication::initializeWorldManager(nox::logic::Logic* logic)
{
	auto world = std::make_unique<nox::logic::world::Manager>(logic);

	/*
	 * We register the ActorComponents Transform and ActorPhysics so that they can be used
	 * as components on an Actor created from a JSON file. To see what name and properties
	 * the components have, see their class documentation (prettier with Doxygen generated documentation).
	 */
	world->registerActorComponent<nox::logic::actor::Transform>();
	world->registerActorComponent<nox::logic::physics::ActorPhysics>();

	/*
	 * We load all actor definitions from the "actor" directory in our assets directory (set up
	 * by the resource cache).
	 * This will recursively search the whole directory tree for JSON files containing Actor definitions
	 * and prepare them for use.
	 *
	 * An Actor definition is defined by a unique combined path and name (similar to the Java packages).
	 * For example the "MovingActor" will only be identified by "MovingActor" since it is in the root directory.
	 * While if there was another Actor JSON definition in a directory called "environment", the unique path name
	 * would be "environment.MovingActor". The whole path must be used when referring to an actor definition.
	 */
	const auto actorDirectory = std::string{"actor"};
	world->loadActorDefinitions(this->getResourceAccess(), actorDirectory);

	auto worldPtr = world.get();

	// Let the logic handle the world handler.
	logic->setWorldManager(std::move(world));

	return worldPtr;
}

/*
 * This will load the JSON file "world/exampleWorld.json" and pass it to the world handler.
 * The world handler will then go through all the Actors in the "actors" array and try to
 * load each of them.
 */
bool WorldApplication::loadWorldFile(nox::logic::IContext* logicContext, nox::logic::world::Manager* worldManager)
{
	/*
	 * This loads a JSON file the same way we've seen in earlier examples (see 04-resources).
	 */
	const auto worldFileDescriptor = nox::app::resource::Descriptor{"world/exampleWorld.json"};
	const auto worldFileHandle = this->getResourceAccess()->getHandle(worldFileDescriptor);

	if (worldFileHandle == nullptr)
	{
		this->log.error().format("Could not load world: %s", worldFileDescriptor.getPath().c_str());
		return false;
	}
	else
	{
		const auto jsonData = worldFileHandle->getExtraData<nox::app::resource::JsonExtraData>();

		if (jsonData == nullptr)
		{
			this->log.error().format("Could not get JSON data for world: %s", worldFileDescriptor.getPath().c_str());
			return false;
		}
		else
		{
			/*
			 * Here the JSON resource was successfully loaded so we pass it to the world manager so that
			 * it can further parse it and load all the actors.
			 */
			auto loader = nox::logic::world::Loader{logicContext};

			if (loader.loadWorld(jsonData->getRootValue(), worldManager) == false)
			{
				this->log.error().format("Failed loading world \"%s\".", worldFileDescriptor.getPath().c_str());
				return false;
			}
		}
	}

	this->log.verbose().format("Loaded world \"%s\"", worldFileDescriptor.getPath().c_str());

	return true;
}

bool WorldApplication::onInit()
{
	if (SdlApplication::onInit() == false)
	{
		return false;
	}

	this->log = this->createLogger();
	this->log.setName("WorldApplication");

	if (this->initializeResourceCache() == false)
	{
		this->log.error().raw("Failed initializing resource cache.");
		return false;
	}

	auto logic = this->initializeLogic();
	auto eventBroadcaster = logic->getEventBroadcaster();

	/*
	 * This time we want to listen for Actor transform changes.
	 */
	this->eventListener.setup(this, eventBroadcaster);
	this->eventListener.addEventTypeToListenFor(nox::logic::actor::TransformChange::ID);
	this->eventListener.startListening();

	this->initializePhysics(logic);
	auto worldManager = this->initializeWorldManager(logic);

	if (this->loadWorldFile(logic, worldManager) == false)
	{
		return false;
	}

	/*
	 * The Logic is paused by default and has to be unpaused so that it can run
	 * all of its subsystems.
	 */
	logic->pause(false);

	return true;
}

void WorldApplication::onDestroy()
{
	this->eventListener.stopListening();
}

void WorldApplication::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	/*
	 * The actor::TransformChange event is broadcasted each time an Actor has its transform changed.
	 * We output the new transform here to see how the Actor is moving.
	 *
	 * Because of the damping the Actor has, the velocity and angle speed will slow down until they finally halt.
	 * When they do no more output will appear since the Actor transform is never changed.
	 *
	 * Plain text output is of course not very exiting, so jump over to the next example to actually see the Actor.
	 */
	if (event->isType(nox::logic::actor::TransformChange::ID))
	{
		auto transformEvent = static_cast<nox::logic::actor::TransformChange*>(event.get());
		this->log.info().format("Actor has moved to %s with angle %0.2f.", glm::to_string(transformEvent->getPosition()).c_str(), transformEvent->getRotation());
	}
}
